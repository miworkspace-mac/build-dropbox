#!/bin/bash -ex

# CONFIG
prefix="Dropbox"
suffix=""
munki_package_name="Dropbox"
display_name="Dropbox"
category="Utilities"
description="This update contains stability and security fixes for the Dropbox desktop application"
#url=`./finder.sh`
url="https://www.dropbox.com/download?full=1&plat=mac"
# download it (-L: follow redirects)
#curl -L -o app.dmg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/15.1 Safari/605.1.15' "${url}"

#removing the 
curl -L -o app.dmg "${url}"


## Mount disk image on temp space
mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $3 } '`


mkdir -p build-root/Applications
app_in_dmg=$(ls -d $mountpoint/*.app)
cp -R $app_in_dmg build-root/Applications

# Obtain version info
version=`/usr/libexec/PlistBuddy -c "Print :CFBundleVersion" $app_in_dmg/Contents/Info.plist`

#creating a components plist to turn off relocation
/usr/bin/pkgbuild --analyze --root build-root/ Component-${munki_package_name}.plist
plutil -replace BundleIsRelocatable       -bool NO Component-${munki_package_name}.plist
plutil -replace BundleHasStrictIdentifier -bool NO Component-${munki_package_name}.plist

#build PKG
/usr/bin/pkgbuild --root build-root/ --identifier edu.umich.izzy.pkg.${munki_package_name} --install-location / --component-plist Component-${munki_package_name}.plist --scripts scripts --version ${version} app.pkg

# cleanup component plist to prevent upload
rm -rf Component-${munki_package_name}.plist

## Find all the appropriate apps, etc, and then turn that into -f's
key_files=`find build-root -name '*.app' -or -name '*.plugin' -or -name '*.prefPane' -or -name '*component' -maxdepth 3 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

## Build pkginfo (this is done through an echo to expand key_files)
echo /usr/local/munki/makepkginfo -m go-w -g admin -o root app.pkg ${key_files} | /bin/bash > app.plist

## Fixup and remove "build-root" from file paths
perl -p -i -e 's/build-root//' app.plist

plist=`pwd`/app.plist

hdiutil detach "${mountpoint}"

# Remove unneeded Installs keys. We want to only detect presence so DropBox can self update.

#/usr/libexec/PlistBuddy -c "Delete :installs:0:CFBundleShortVersionString" app.plist
#/usr/libexec/PlistBuddy -c "Delete :installs:0:CFBundleVersion" app.plist
#/usr/libexec/PlistBuddy -c "Delete :installs:0:CFBundleIdentifier" app.plist
#/usr/libexec/PlistBuddy -c "Delete :installs:0:CFBundleName" app.plist
#/usr/libexec/PlistBuddy -c "Delete :installs:0:version_comparison_key" app.plist
#/usr/libexec/PlistBuddy -c "Delete :installs:0:minosversion" app.plist

#/usr/libexec/PlistBuddy -c "Set :installs:0:type file" app.plist

# Obtain version info
version=`defaults read "${plist}" version`

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.pkg"
defaults write "${plist}" minimum_os_version "10.10.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"
defaults write "${plist}" category "${category}"
defaults write "${plist}" description -string "${description}"
defaults write "${plist}" RestartAction "RequireLogout"

# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv app.pkg   ${prefix}-${version}${suffix}.pkg
mv app.plist ${prefix}-${version}${suffix}.plist
