#!/bin/bash -ex

VERSION=`./finder.sh`

if [ "x${VERSION}" != "x" ]; then
    echo version: "${VERSION}"
    echo "${VERSION}" > current-version
	rm -rf version_check.dmg
fi

# Update to handle distributed builds
if cmp current-version old-version; then
    # Files are identical, exit 1 to NOT trigger the build job
	rm -rf version_check.dmg
    exit 1
else
    # Files are different - copy marker, exit 0 to trigger build job
    cp current-version old-version
	rm -rf version_check.dmg
    exit 0
fi
